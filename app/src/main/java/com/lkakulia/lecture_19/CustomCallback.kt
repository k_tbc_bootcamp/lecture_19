package com.lkakulia.lecture_19

interface CustomCallback {
    fun onFailure(error: String)
    fun onResponse(response: String)
}