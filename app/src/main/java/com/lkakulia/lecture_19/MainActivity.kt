package com.lkakulia.lecture_19

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log.d
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private var items = mutableListOf<DataModel>()
    private lateinit var adapter: RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun getData() {
        HttpRequest.getRequest(HttpRequest.PATH, object : CustomCallback {
            override fun onFailure(error: String) {
                d("onFailure", " ${error}")
            }

            override fun onResponse(response: String) {
                swipeRefreshLayout.isRefreshing = false
                parseJson(response)
            }

        })
    }

    private fun init() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(items)
        recyclerView.adapter = adapter

        swipeRefreshLayout.isRefreshing = true
        getData()

        swipeRefreshLayout.setOnRefreshListener({
            swipeRefreshLayout.isRefreshing = true
            refresh()

            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                getData()
                adapter.notifyDataSetChanged()
            }, 2000)
        })
    }

    private fun parseJson(response: String) {
        val json = JSONObject(response)

        if (json.has("competitions")) {
            val jsonArray = json.getJSONArray("competitions")
            d("array", " ${jsonArray}")


            for (i in 0 until jsonArray.length()) {
                val dataModel = DataModel()
                val jsonObject = jsonArray.getJSONObject(i).getJSONObject("area")

                if (jsonObject.has("id"))
                    dataModel.id = jsonObject.getInt("id")
                if (jsonObject.has("name"))
                    dataModel.name = jsonObject.getString("name")
                if (jsonObject.has("countryCode"))
                    dataModel.countryCode = jsonObject.getString("countryCode")
                if (jsonObject.has("ensignUrl"))
                    dataModel.ensignUrl = jsonObject.getString("ensignUrl")

                adapter.notifyDataSetChanged()
                items.add(dataModel)

            }

        }
    }

    private fun refresh() {
        items.clear()
        adapter.notifyDataSetChanged()
    }

}
